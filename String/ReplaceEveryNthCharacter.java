public class ReplaceEveryNthCharacter {
    public static void main(String args[]) {
    // Replace every Nth character
      int nn =3;
    //   System.out.println(nn);
      String inputString = "FauzanMasud";
    //   System.out.println(inputString);
      String replacementChar = "_";
      char replacementChar1 = replacementChar.charAt(0);
    //   System.out.println(replacementChar);
    // Check if N is greater than 0 and less than or equal to the length of the string
      if (nn > 0 && nn <= inputString.length()) {
        //   System.out.println("Masuk IF");
            // Convert the string to a char array to allow modification
            char[] charArray = inputString.toCharArray();
            // System.out.println("Setelah charArray");
            // Replace every Nth character
            for (int i = nn - 1; i < charArray.length; i += nn) {
            // for (int i = nn - 1; i < inputString.length(); i += nn)  {
                // System.out.println("Setelah for");
                charArray[i] = replacementChar1;
            }
            System.out.println(new String(charArray));
            // // Convert the char array back to a string
            // return new String(charArray);
        } else {
            // If N is not in a valid range, return the original string
            System.out.println("Invalid value of N. N should be greater than 0 and less than or equal to the length of the string.");
            // return inputString;
        }
    }
}
