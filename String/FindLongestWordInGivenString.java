import java.util.Arrays;
import java.util.Comparator;

public class FindLongestWordInGivenString {
    public static void main(String[] args) {
        //java 8
        String s = "Today is the happiest day of my life";
        String longest = Arrays.stream(s.split(" "))
                .max(Comparator.comparingInt(String::length))
                .orElse(null);
        System.out.println(longest);

        // //java 7
        // String s= "Today is the happiest day of my life";
        // String [] word = s.split(" ");
        // String longest = "";
        // for(int i = 0; i < word.length; i++){
        //     if(word[i].length() >= longest.length()){
        //           longest = word[i];
        //     } 
        // }
        // System.out.println(longest);
    }
}
